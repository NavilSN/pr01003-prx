/** General **/
$(document).ready(function () {
    $('.hamburger-menu').on('click', function () {
        $('.bar').toggleClass('animate');
        $('.overlay').toggleClass('open');
        $('.stage').toggleClass('blur');
        $('.logo-section').toggleClass('blur');
        $('.fc-anchor').toggleClass('blur');
        $('.page-title-section').toggleClass('blur'); 
    });

});



function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}

 var cta_scroll = false;


 $(window).on('scroll', function () {
     var ctaLoop = document.querySelectorAll('#cta .reveal-video');
     ctaLoop[0].classList.add('cta-video');
     if (isScrolledIntoView('#cta') && !cta_scroll) {
         cta_scroll = true;
        let revealVideo = document.querySelector('#cta .reveal-video');
        revealVideo.onended = function () {
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');
            $(this).next().get(0).play();
        };
     }
 });


/** About Us **/
$(function () {
    if ($('body').is('.about-us')) {
        let revealVideo = document.querySelector('.banner-video.reveal-video');
        revealVideo.onended = function () {
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');
            $(this).next().get(0).play();
        };

        var about_desc_firstScroll = false;
        var about_mission_firstScroll = false;
        var about_vision_firstScroll = false;


        $(window).on('scroll', function () {
            var videoNoLoop = document.querySelectorAll('.about-description .reveal-video');
            videoNoLoop[0].classList.add('vision-video');
            videoNoLoop[1].classList.add('mission-video');
            if (isScrolledIntoView('.about-description') && !about_desc_firstScroll) {
                document.querySelector('.vision-video').play();
                document.querySelector('.mission-video').play();
            }   
            if (isScrolledIntoView('.about-description') && !about_desc_firstScroll) {
                about_desc_firstScroll = true;
                $('.about-description .txt-title').addClass('reveal-text');
                $('.about-description .txt-title').css('opacity', '1');
                $('.about-description .txt-content').addClass('reveal-content');
                $('.about-description .txt-content').css('opacity', '1');
            }
        });
    }
});

/** Contact Us **/
$(function () {
    if ($('body').is('.contact-us')) {
        // var contact_form_firstScroll = false;

        $(window).on('scroll', function () {
            if (isScrolledIntoView('.contact-form-section') && !contact_form_firstScroll) {
                contact_form_firstScroll = true;
                $('.contact-form-section .txt-title').addClass('reveal-text');
                $('.contact-form-section .txt-title').css('opacity', '1');
                $('.contact-form-section .txt-content').addClass('reveal-content');
                $('.contact-form-section .txt-content').css('opacity', '1');
            }
        });
        /** Contact Form Validation **/
        $(document).ready(function (e) {
            $('.error').hide();

            var status = false;

            $('#contact_submit').on('click', function (e) {
                e.preventDefault();
                $('.contact_form .ip-controls').each(function () {
                    if ($('input', this).val() == '') {
                        $('input', this).css('border', '1px solid red');
                        $('input', this).next().show();
                        status = true;
                    }
                    if ($('textarea', this).val() == '') {
                        $('textarea', this).css('border', '1px solid red');
                        $('textarea', this).next().show();
                        status = false;
                    }
                    if ($(this).attr('type') == 'email') {
                        var pattern = new RegExp(/^[a-zA-Z0-9.!#$%&amp;'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
                        //console.log($(this).val());
                        if (pattern.test($(this).val())) {
                            $(this).css('border', '1px solid #006400');
                            $(this).next().hide();
                            //console.log('Matched !!!');
                            status = false;
                        } else {

                            //console.log('Not Matched !!!');
                            $(this).css('border', '1px solid red');
                            $(this).next().show();
                            //console.log($(this).next().text());
                            status = true;

                        }
                    }
                    if ($(this).attr('type') == 'tel') {
                        //							/^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/
                        var tel_pattern = new RegExp(/^[0-9]{10}$/);
                        //console.log($(this).val());

                        if (tel_pattern.test($(this).val())) {
                            $(this).css('border', '1px solid #006400');
                            $(this).next().hide();
                            //console.log('Matched !!!');
                            status = false;
                        } else {
                            //console.log('Not Matched !!!');
                            $(this).css('border', '1px solid red');
                            $(this).next().show();
                            status = true;
                        }
                    }
                });
            });
            $('.contact_form .ip-controls').each(function () {
                $('input', this).each(function () {
                    $(this).on('blur', function () {
                        if ($(this).val() == '') {
                            $(this).css('border', '1px solid red');
                            $(this).next().show();
                            status = true;
                        } else {
                            $(this).css('border', '1px solid #006400');
                            $(this).next().hide();
                            status = false;
                        }

                        if ($(this).attr('type') == 'email') {
                            var pattern = new RegExp(/^[a-zA-Z0-9.!#$%&amp;'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/);
                            //console.log($(this).val());

                            if (pattern.test($(this).val())) {
                                $(this).css('border', '1px solid #006400');
                                $(this).next().hide();
                                //console.log('Matched !!!');
                                status = false;
                            } else {
                                //console.log('Not Matched !!!');
                                $(this).css('border', '1px solid red');
                                $(this).next().show();
                                status = true;
                            }
                        }

                        if ($(this).attr('type') == 'tel') {
                            //							/^(\+91[\-\s]?)?[0]?(91)?[789]\d{9}$/
                            var tel_pattern = new RegExp(/^[0-9]{10}$/);
                            //console.log($(this).val());

                            if (tel_pattern.test($(this).val())) {
                                $(this).css('border', '1px solid #006400');
                                $(this).next().hide();
                                //console.log('Matched !!!');
                                status = false;
                            } else {
                                //console.log('Not Matched !!!');
                                $(this).css('border', '1px solid red');
                                $(this).next().show();
                                status = true;
                            }
                        }
                    });
                });

                $('textarea', this).each(function () {
                    $(this).on('blur', function () {
                        if ($(this).val() == '') {
                            $(this).css('border', '1px solid red');
                            $(this).next().show();
                        } else {
                            $(this).css('border', '1px solid #006400');
                            $(this).next().hide();
                        }
                    });
                });
            });
        });
    }
});

/** Services **/
$(function () {
    if ($('body').is('.services')) {
        let revealVideo = document.querySelector('.reveal-video');
        revealVideo.onended = function () {
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');
            $(this).next().get(0).play();
        };

        var contact_form_firstScroll = false;
        var partners_firstScroll = false;

        $(window).on('scroll', function () {
            if (isScrolledIntoView('.services-description') && !contact_form_firstScroll) {
                contact_form_firstScroll = true;
                $('.services-description .txt-title').addClass('reveal-text');
                $('.services-description .txt-title').css('opacity', '1');
                $('.services-description .txt-content').addClass('reveal-content');
                $('.services-description .txt-content').css('opacity', '1');
            }
        });
    }
});

/** Projects **/
$(function () {
    if ($('body').is('.projects')) {
        let revealVideo = document.querySelector('.reveal-video');
        revealVideo.onended = function () {
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');
            $(this).next().get(0).play();
        };

        var projects_title_firstScroll = false;

        $(window).on('scroll', function () {
            if (isScrolledIntoView('.projects-description') && !projects_title_firstScroll) {
                projects_title_firstScroll = true;
                $('.projects-description .txt-title').addClass('reveal-text');
                $('.projects-description .txt-title').css('opacity', '1');
                $('.projects-description .txt-content').addClass('reveal-content');
                $('.projects-description .txt-content').css('opacity', '1');
            }
        });
    }
});

/** IT Solutions **/
$(function () {
    if ($('body').is('.it-solutions')) {
        let revealVideo = document.querySelector('.reveal-video');
        revealVideo.onended = function () {
            $(this).css('display', 'none');
            $(this).next().css('display', 'block');
            $(this).next().get(0).play();
        };

        var solutions_title_firstScroll = false;
        var solutions_content_firstScroll = false;
        var partners_firstScroll = false;

        var sol1_firstScroll = false;
        var sol2_firstScroll = false;
        var sol3_firstScroll = false;
        $(window).on('scroll', function () {
            if (isScrolledIntoView('.services-description') && !solutions_title_firstScroll) {
                solutions_title_firstScroll = true;
                $('.services-description .txt-title').addClass('reveal-text');
                $('.services-description .txt-title').css('opacity', '1');
                $('.services-description .txt-content').addClass('reveal-content');
                $('.services-description .txt-content').css('opacity', '1');
            }
        });
    }
});